let selected;
let animating = true;

const today = new Date();
selected = today;

const animations = [];
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const week = [
  [
    {
      id: 10,
      title: "Church",
      time: "8:00 am"
    },
    {
      id: 11,
      title: "Bible Study",
      time: "5:00 pm"
    }
  ],
  [
    {
      id: 1,
      title: "Meeting with Client",
      time: "2:00 pm"
    },
    {
      id: 2,
      title: "Running with Teammate",
      time: "6:00 pm"
    }
  ],
  [
    {
      id: 3,
      title: "Meeting with Boss",
      time: "11:00 am"
    }
  ],
  [
    {
      id: 4,
      title: "Dentist Appointment",
      time: "8:00 am"
    },
    {
      id: 5,
      title: "Movies with Friends",
      time: "8:00 pm"
    }
  ],
  [
    {
      id: 6,
      title: "Walk neighbor's dog",
      time: "5:00 am"
    }
  ],
  [
    {
      id: 7,
      title: "Doctor Appointment",
      time: "2:00 pm"
    },
    {
      id: 8,
      title: "Movies with Friends",
      time: "8:00 pm"
    },
    {
      id: 30,
      title: "Doctor Appointment",
      time: "2:00 pm"
    },
    {
      id: 32,
      title: "Movies with Friends",
      time: "8:00 pm"
    },
    {
      id: 646,
      title: "Doctor Appointment",
      time: "2:00 pm"
    },
    {
      id: 34,
      title: "Movies with Friends",
      time: "8:00 pm"
    },
    {
      id: 65,
      title: "Doctor Appointment",
      time: "2:00 pm"
    },
    {
      id: 654,
      title: "Movies with Friends",
      time: "8:00 pm"
    },
    
    {
      id: 65,
      title: "Doctor Appointment",
      time: "2:00 pm"
    },
    {
      id: 654,
      title: "Movies with Friends",
      time: "8:00 pm"
    }
  ],
  [
    {
      id: 9,
      title: "Tennis with Roommate",
      time: "7:00 pm"
    }
  ]
];

const inc = () => {
  const nxt = new Date(selected.getFullYear(), selected.getMonth()+1, 1);
  // TODO: change month
};

const dec = () => {
  const prev = new Date(selected.getFullYear(), selected.getMonth()-1, 1);
  // TODO: change month
};

const maxDays = (m, y) => {
  switch (m) {
    case 0:
    case 2:
    case 4:
    case 6:
    case 7:
    case 9:
    case 11:
      return 31;
    case 1:
      if (!(y % 4)) return 29;
      else return 28;
    default:
      return 30;
  }
};

const event = (title, time, id, day) => `
<div class="header details">
  <div class='inner'>
    <div class='email-details'>
      <p>
        <b>${title}</b>
        <br>
        <small>${time}</small>
      </p>
      <span class='delete' onClick='trash(event, ${id}, ${day})'>X</span>
    </div>
  </div>
</div>
`;

const cal_day = (m, d, y) => `
<li class='case' onClick="select(${m}, ${d}, ${y})">${d}</li>
`;

const cal = (m, y) => {
  let build = cal_day(m-1, maxDays(m-1), y);
  for (let i = 1; i <= maxDays(m, y); i++) {
    build += cal_day(m, i, y);
  } return build += cal_day(m+1, 1, y);
};

const addEvents = (parent, e=[], day) => {
  parent.innerHTML = e.reduce((a, {id, title, time}) => a + '\n' + event(title, time, id, day), '');
};

const app = document.getElementById("app");
const toggle = document.getElementById("toggle");

const add = document.getElementById('add');
const month = document.getElementById('month');
const m_name = document.getElementById('month-name');
const current = document.getElementById('current');
const calendar = document.getElementById('calendar');

const days = [
  document.getElementById('day-0'),
  document.getElementById('day-1'),
  document.getElementById('day-2'),
  document.getElementById('day-3'),
  document.getElementById('day-4'),
  document.getElementById('day-5'),
  document.getElementById('day-6')
];

const date = document.getElementById('date');
date.innerText = selected.toDateString();

const animate = (button, day) => {
  if (animating && animations.length) return;
  animating = true;
  
  days.forEach(d => d.classList.remove('selected'));
  days[day].classList.add('selected');  
  date.classList.add('no-color');
  
  if (selected.getDay() > day) {
    const tmp = new Date(selected.toDateString());
    tmp.setDate(selected.getDate() - (selected.getDay() - day));
    date.innerText = tmp.toDateString();
    selected = tmp;
  } else if (selected.getDay() < day) {
    const tmp = new Date(selected.toString());
    tmp.setDate(selected.getDate() + (day - selected.getDay()));
    date.innerText = tmp.toDateString();
    selected = tmp;
  } else {
    date.innerText = selected.toDateString();
  }
  
  calendar.innerHTML = cal(selected.getMonth(), selected.getFullYear());
  m_name.innerText = months[selected.getMonth()];
  
  const wrapper = button.querySelector('.bar-wrapper');
  wrapper.classList.add('bar-wid');
  
  const grow = button.querySelector('.bar-grow');
  const right = button.querySelector('.bar-date');  
  const left = button.querySelector('.bar-left');
  
  const size = wrapper.offsetTop;
  const top = `margin-top: -${size-10}px;`;
  
  if (!wrapper.style || !wrapper.style.marginTop) {
    wrapper.style = top;
  
    const off = wrapper.offsetTop;
    const height = `height: ${off + size - 45}px;`;
    const min = `min-height: ${off + size - 45}px;`;

    left.style = height + min;
    right.style = height + min;
  }
  
  setTimeout(() => {
    left.classList.add("bar-right");
    setTimeout(() => {
      grow.classList.add("bar-up");
      setTimeout(() => {
        right.classList.add("bar-right");
        setTimeout(() => {
          date.classList.remove("no-color");
          right.classList.remove("bar-right");
          setTimeout(() => {
            right.classList.add("none");
            setTimeout(() => {
              grow.classList.remove("bar-up");
              right.classList.remove("none");
              setTimeout(() => {
                left.classList.remove("bar-right");
                wrapper.classList.remove('bar-wid');
                current.classList.remove('none');
                animating = false;
              }, 300);
            }, 300);
          }, 250);
        }, 300);
      }, 300);
    }, 300);
  }, 300);
};

const loadCurrent = (day) => {
  const events = week[day];
  current.classList.add('none');
  
  animate(days[day], day);
  addEvents(current, events, day);
};

loadCurrent(selected.getDay());
const day = (e, d) => {
  animations.push(d);
  if (animations.length === 1) {
    loadCurrent(d);
  } setTimeout(() => {
    if (animations.length > 1) {
      loadCurrent(animations[animations.length - 1]);
    } while (animations.length) animations.pop();
  }, 1500);
};

const onLight = e => {
  if (app.classList.contains("light")) {
    month.classList.remove('light');
    app.classList.remove("light");
    add.classList.remove("light");
    add.classList.add("dark");
    app.classList.add("dark");
    month.classList.add('dark');
    toggle.classList.remove("active");
  } else {
    month.classList.remove('dark');
    app.classList.remove("dark");
    add.classList.remove("dark");
    add.classList.add("light");
    app.classList.add("light");
    month.classList.add('light');
    toggle.classList.add("active");
  }
};

const trash = (e, id, day) => {
  week[day] = week[day].filter(d => d.id != id);
  addEvents(current, week[day], day);
};

const onMonth = (e) => {
  calendar.innerHTML = cal(selected.getMonth(), selected.getFullYear());
  m_name.innerText = months[selected.getMonth()];
  
  month.classList.add('off-page');
  setTimeout(() => {
    month.classList.remove('none');
    month.classList.add('on-page');
  }, 300);
};

const onAdd = (e) => {
  add.classList.add('off-page');
  setTimeout(() => {
    add.classList.remove('none');
    add.classList.add('on-page');
  }, 300);
};

const onCancel = (event) => {
  add.classList.remove('on-page');
  setTimeout(() => {
    add.classList.add('none');
    day(null, selected.getDay());
  }, 300);
};

const onSubmit = (event) => onCancel(event);

const select = (m, d, y) => {
  selected = new Date(y, m, d);
  month.classList.remove('on-page');
  setTimeout(() => {
    month.classList.add('none');
    day(null, selected.getDay());
  }, 300);
};